package com.nagarro.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.nagarro.service.Service;

//controller for managing request and communicating files ;
@Controller
public class AddController {

	@RequestMapping(value = "/login1", method = RequestMethod.POST)
	public ModelAndView signIn(@RequestParam String username, @RequestParam String password) {
		ModelAndView mv = new ModelAndView();
		Service service = new Service();
		if (service.verifyUser(username, password)) {
			mv.setViewName("success");
		} else {
			String invalidUser = "Please enter username or password";
			mv.setViewName("LoginFailed");
			mv.addObject("invalidUser", invalidUser);
		}
		return mv;
	}

	@RequestMapping(value = "/findproduct", method = RequestMethod.POST)
	public ModelAndView signIn(@RequestParam String colour, @RequestParam String size, @RequestParam String gender,
			@RequestParam String outputPrefrence) {
		ModelAndView mv = new ModelAndView();
		Service service = new Service();
		List<Object> record = service.productSerach(colour, size, gender, outputPrefrence);
		int m = 0;
		for (Object data : record) {
			System.out.println(data);
			m++;
		}
		if (m != 0) {
			mv.setViewName("record");
			mv.addObject("record", record);
		} else {
			mv.setViewName("enteragain");
		}

		return mv;
	}

	@RequestMapping(value = "/login2", method = RequestMethod.POST)
	public ModelAndView logout() {
		ModelAndView mv = new ModelAndView();
		String invalidUser = "Please enter username or password";
		mv.setViewName("LoginFailed");
		mv.addObject("invalidUser", invalidUser);
		return mv;
	}
}
