package com.nagarro.Csv;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.nagarro.service.Service;
import com.nagarro.training.entity.AdidasDetails;
import com.nagarro.training.entity.NikeDetails;
import com.nagarro.training.entity.PumaDetails;

public class CsvOperation implements CsvInterface {
	ArrayList<String> arr;
	Service sv = new Service();

	@Override
	public void storeCSV(String filename) {
		try {
			BufferedReader br = new BufferedReader(new FileReader(filename));
			CSVParser parser = CSVFormat.DEFAULT.withDelimiter(',').withHeader().parse(br);
			{
				int i = 0;
				for (CSVRecord record : parser) {
					String line = record.get(i);
					if (!line.isEmpty()) {

						StringTokenizer token = new StringTokenizer(line, "|");
						arr = new ArrayList<String>(line.length());
						while (token.hasMoreTokens()) {
							arr.add(token.nextToken());
						}

					}
					try {
						if (filename.equalsIgnoreCase("C:\\ashishjaiswal\\Project\\Adidas.csv")
								&& (arr.get(0).equalsIgnoreCase("id") != true)) {
							AdidasDetails ad = new AdidasDetails(arr.get(0), arr.get(1), arr.get(2), arr.get(3),
									arr.get(4), arr.get(5), arr.get(6), arr.get(7));
							sv.Calling(ad);

						}

						else if (filename.equalsIgnoreCase("C:\\ashishjaiswal\\Project\\Nike.csv")
								&& (arr.get(0).equalsIgnoreCase("id") != true)) {
							NikeDetails nd = new NikeDetails(arr.get(0), arr.get(1), arr.get(2), arr.get(3), arr.get(4),
									arr.get(5), arr.get(6), arr.get(7));
							sv.Calling(nd);
						}

						else if (filename.equalsIgnoreCase("C:\\ashishjaiswal\\Project\\Puma.csv")
								&& (arr.get(0).equalsIgnoreCase("id") != true)) {
							PumaDetails pd = new PumaDetails(arr.get(0), arr.get(1), arr.get(2), arr.get(3), arr.get(4),
									arr.get(5), arr.get(6), arr.get(7));
							sv.Calling(pd);
						}
					} catch (Exception e) {
						e.printStackTrace();

					}
				}

			}

		} catch (Exception e) {
			System.out.println(e);
		}

	}

	@Override
	public List fetchRecord(String colour, String size, String gender, String outputPrefrence) {
		List<Object> al = sv.Fetchdata(colour, size, gender, outputPrefrence);
		return al;
	}

}
