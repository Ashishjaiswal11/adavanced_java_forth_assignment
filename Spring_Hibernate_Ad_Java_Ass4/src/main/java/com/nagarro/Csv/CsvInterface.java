package com.nagarro.Csv;

import java.io.FileNotFoundException;
import java.util.List;

//interface for storecsv csv file and fetching record 
public interface CsvInterface {
	public void storeCSV(String name) throws FileNotFoundException;

	List fetchRecord(String colour, String size, String gender, String outputPrefrence);
}
