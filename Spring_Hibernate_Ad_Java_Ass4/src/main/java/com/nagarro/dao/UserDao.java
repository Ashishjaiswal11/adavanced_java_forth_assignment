package com.nagarro.dao;

import java.util.List;

import com.nagarro.training.entity.AdidasDetails;
import com.nagarro.training.entity.NikeDetails;
import com.nagarro.training.entity.PumaDetails;
import com.nagarro.training.entity.User;

//interface for verifying user and saving CSV files into database;
public interface UserDao {
	public User searchUser(int eno);

	public void saveData(AdidasDetails data);

	public void saveData(PumaDetails data);

	public void saveData(NikeDetails data);

	public List fetchMatchingRecord(String colour, String size, String gender, String outputPrefrence);

}
