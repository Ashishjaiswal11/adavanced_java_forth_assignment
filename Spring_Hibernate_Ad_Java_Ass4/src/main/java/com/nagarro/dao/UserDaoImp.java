package com.nagarro.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.nagarro.training.entity.AdidasDetails;
import com.nagarro.training.entity.NikeDetails;
import com.nagarro.training.entity.PumaDetails;
import com.nagarro.training.entity.User;

//implementation of all database related methods using HibernateTemplate Class.
public class UserDaoImp implements UserDao {
	String status = "";
	private HibernateTemplate hibernateTemplate;

	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}

	@Transactional
	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}

	public UserDaoImp() {
		// TODO Auto-generated constructor stub
	}

	@Transactional
	@Override
	public User searchUser(int eno) {
		User user = hibernateTemplate.get(User.class, eno);
		return user;
	}

	@Transactional
	@Override
	public void saveData(AdidasDetails data) {
		System.out.println(data);
		hibernateTemplate.save(data);

	}

	@Transactional
	@Override
	public void saveData(PumaDetails data) {
		System.out.println(data);
		hibernateTemplate.save(data);

	}

	@Transactional
	@Override
	public void saveData(NikeDetails data) {
		System.out.println(data);
		hibernateTemplate.save(data);

	}

	@Transactional
	@Override
	public List fetchMatchingRecord(String colour, String size, String gender, String outputPrefrence) {
		ArrayList<Object> al = new ArrayList<>();
		List<AdidasDetails> ad = hibernateTemplate.loadAll(AdidasDetails.class);
		for (AdidasDetails l : ad) {
			if (l.getColour().equalsIgnoreCase(colour) && l.getSize().equalsIgnoreCase(size)
					&& l.getGender().equalsIgnoreCase(gender)) {
				al.add(l);
			}
		}
		List<NikeDetails> nd = hibernateTemplate.loadAll(NikeDetails.class);
		for (NikeDetails l : nd) {
			if (l.getColour().equalsIgnoreCase(colour) && l.getSize().equalsIgnoreCase(size)
					&& l.getGender().equalsIgnoreCase(gender)) {
				al.add(l);
			}
		}
		List<PumaDetails> pd = hibernateTemplate.loadAll(PumaDetails.class);
		for (PumaDetails l : pd) {
			if (l.getColour().equalsIgnoreCase(colour) && l.getSize().equalsIgnoreCase(size)
					&& l.getGender().equalsIgnoreCase(gender)) {
				al.add(l);
			}
		}
		return al;

	}

}
