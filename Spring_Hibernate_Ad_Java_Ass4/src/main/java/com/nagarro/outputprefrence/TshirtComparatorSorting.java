package com.nagarro.outputprefrence;

import java.util.Comparator;

//using comparator and methods for output preference;
public class TshirtComparatorSorting {

	// "Creating Comparator to by sort by Price.");
	public Comparator<Object> getTshirtComparatorbyPrice() {

		return new Comparator<Object>() {
			@Override
			public int compare(Object o1, Object o2) {
				return compare(o1.getClass(), o2.getClass());
			}
		};

	}

	// "Creating Comparator to by sort by rating."

	public Comparator<Object> getTshirtComparatorbyRating() {

		return new Comparator<Object>() {
			@Override
			public int compare(Object o1, Object o2) {
				return compare(o1.getClass(), o2.getClass());
			}
		};

	}

	// Comparator class that sort by price and rating duration.

	public Comparator<Object> getTshirtComparatorPriceRating() {

		return new Comparator<Object>() {
			@Override
			public int compare(Object o1, Object o2) {
				int sortLevel1 = compare(o1.getClass(), o2.getClass());
				if (sortLevel1 == 0) {
					int sortLevel2 = compare(o1.getClass(), o2.getClass());
					return sortLevel2;
				}
				return sortLevel1;
			}
		};

	}
}
